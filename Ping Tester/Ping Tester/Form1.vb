﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Net
Imports System
Imports System.Net.NetworkInformation
Imports System.Text
Imports System.ComponentModel
Imports System.Threading

Public Class Form1
    ''' <summary>
    ''' Déclaration de variable publique 
    ''' </summary>
    Private Const V As String = "Play"
    Public compt, a As Integer
    Public array_ip(1000) As String
    Public fichier, adresse_fichier As String
    Public rep As Integer
    Public var_fichier1, var_fichier2 As String
    Dim Tabl(1300) As String
    Public value1 As String


    ''' <summary>
    ''' Sub de lancement du Form
    ''' </summary>
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        adresse_fichier = ""
        compt = 0
        Label0.Text = ""
        Label1.Text = ""
        Label4.Text = ""
        TextBox2.Multiline = True
        Me.Height = 555
        Me.Width = 340


    End Sub

    ''' <summary>
    ''' Deux bouton de l'onglet "Ping ip"
    ''' </summary>
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles ping_ip_test.Click
        Dim rx As New Regex("^(?:(?:25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)(?(?=\.?\d)\.)){4}$")

        If Not rx.IsMatch(txt_ping.Text) Then
            MessageBox.Show("L'adresse IP n'est pas au bon format !")
            txt_ping.Clear()
            txt_ping.Focus()
            GoTo Fin0
        End If

        Dim ip_var As Object

        If (txt_ping.Text <> "") Then
            ip_var = txt_ping.Text
            Label0.Text = If(My.Computer.Network.Ping(ip_var, 1000), "IP FOUND", "IP NOT FOUND")
            ping_ip(ip_var)
            cmd()
        End If
Fin0:
    End Sub


    ''' <summary>
    ''' Bouton d'importation du fichier txt
    ''' </summary>
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Importer.Click
        Test_ping.BackColor = Color.Transparent
        Export.BackColor = Color.Transparent
        Fimporter()
    End Sub

    ''' <summary>
    ''' Bouton de ping multi
    ''' </summary>
    Private Sub Test_ping_Click(sender As Object, e As EventArgs) Handles Test_ping.Click
        Export.BackColor = Color.Transparent
        Ftest_ping()
    End Sub

    ''' <summary>
    ''' Bouton d'export
    ''' </summary>
    Private Sub Export_Click(sender As Object, e As EventArgs) Handles Export.Click
        Fexport()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles parcourir.Click
        Ffichier()
    End Sub

    ''' <summary>
    ''' Timer pour l'auto ping
    ''' </summary>
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim i As Integer = 0
        'If textbox_adress_fichier.Text = "" Then
        '    Fadresse_fichier()
        '    If Fadresse_fichier() = "0" Then
        '        GoTo Fin
        '    End If
        'End If
        TabControl1.SelectedTab = TabPage1
        Ftest_ping()
        rep = vbYes
        Fexport()
        TabControl1.SelectedTab = TabPage4
        TextBox2.Text = fichier & vbCrLf & vbNewLine & vbCrLf
        TextBox2.Refresh()
        'Fin:
    End Sub

    ''' <summary>
    ''' Play/Stop auto ping
    ''' </summary> 
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            If ListView1.Items.Count = 0 Then
                Fimporter()
            End If

            Fadresse_fichier()
            textbox_adress_fichier.Text = adresse_fichier
            CheckBox1.BackColor = Color.Green
            CheckBox1.Text = "Play"
            Timer1.Start()
        End If
        If CheckBox1.Checked = False Then
            CheckBox1.BackColor = Color.Red
            CheckBox1.Text = "Stop"
            Timer1.Stop()
        End If

    End Sub

    ''' <summary>
    ''' Choix du fichier 1 à comparer
    ''' </summary>
    Private Sub Fichier_2_Click(sender As Object, e As EventArgs) Handles Fichier_2.Click
        OpenFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
        OpenFileDialog1.FilterIndex = 1
        OpenFileDialog1.RestoreDirectory = True
        ' On affiche le formulaire et on teste si l'utilisateur a bien sélectionné un fichier.
        ' L'utilisateur aura donc cliqué sur le bouton OK.
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            ' Récupère le chemin complet du fichier sélectionné par l'utilisateur
            var_fichier2 = OpenFileDialog1.FileName
            Text_fichier2.SelectionStart = Len(Text_fichier2.Text)
            Text_fichier2.Text = OpenFileDialog1.SafeFileName
            ' Affiche le nom du fichier (seulement) sélectionné, à l'utilisateur
            'MsgBox("Vous avez sélectionné le fichier : " & OpenFileDialog1.SafeFileName)

        Else
            ' Si l'utilisateur n'a pas sélectionné de fichier, on lui affiche un avertissement
            MsgBox("Aucun fichier n'a été sélectionné", MsgBoxStyle.Exclamation, "Aucun fichier sélectionné")
        End If

        ' Vérifie que le fichier existe
        OpenFileDialog1.CheckFileExists = True
        ' Vérifie que le chemin d'accès du fichier existe
        OpenFileDialog1.CheckPathExists = True
    End Sub
    ''' <summary>
    ''' Choix du fichier 3 à comparer
    ''' </summary>
    Private Sub Button2_Click_2(sender As Object, e As EventArgs) Handles Fichier_1.Click
        OpenFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
        OpenFileDialog1.FilterIndex = 1
        OpenFileDialog1.RestoreDirectory = True

        ' On affiche le formulaire et on teste si l'utilisateur a bien sélectionné un fichier.
        ' L'utilisateur aura donc cliqué sur le bouton OK.
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            ' Récupère le chemin complet du fichier sélectionné par l'utilisateur
            var_fichier1 = OpenFileDialog1.FileName
            Text_fichier1.SelectionStart = Len(Text_fichier1.Text)
            Text_fichier1.Text = OpenFileDialog1.SafeFileName
            ' Affiche le nom du fichier (seulement) sélectionné, à l'utilisateur

            'MsgBox("Vous avez sélectionné le fichier : " & OpenFileDialog1.SafeFileName)

        Else
            ' Si l'utilisateur n'a pas sélectionné de fichier, on lui affiche un avertissement
            MsgBox("Aucun fichier n'a été sélectionné", MsgBoxStyle.Exclamation, "Aucun fichier sélectionné")
        End If

        ' Vérifie que le fichier existe
        OpenFileDialog1.CheckFileExists = True
        ' Vérifie que le chemin d'accès du fichier existe
        OpenFileDialog1.CheckPathExists = True
    End Sub


    ''' <summary>
    ''' Bouton comparer
    ''' </summary>
    Private Sub Button1_Click_2(sender As Object, e As EventArgs) Handles Button1.Click
        File2Compare(var_fichier1, var_fichier2)
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Importer.BackColor = Color.Transparent
        Importer.ForeColor = Color.Black
        Test_ping.BackColor = Color.Transparent
        Test_ping.ForeColor = Color.Black
        Export.BackColor = Color.Transparent
        Export.ForeColor = Color.Black
    End Sub


    ''''''''''''''''''''''''''''''''''''''''''''''''''''FONCTION''''''''''''''''''''''''''''''''''''''''''''''''


    ''' <summary>
    ''' Fonction pour importer le fichier txt
    ''' </summary>
    ''' <returns></returns>
    Function Fimporter()
        Dim lectureFichier As String
        If TextBox1.Text = "" Then
            Ffichier()
        End If
        lectureFichier = TextBox1.Text
        compt = 0
        ListView1.Clear()
        ListView1.View = View.Details
        ListView1.Columns.Add("IP adress", 150)
        ListView1.Columns.Add("Ping test", 150)

        Dim aline() As String

        'Dim ofile As IO.StreamReader
        Dim sPath As String
        Dim objReader As IO.StreamReader
        Dim sLine As String
        Dim arrText As New ArrayList()
        Dim sSeperator As String
        Dim lvi As ListViewItem
        sPath = lectureFichier
        If sPath = "" Then
            GoTo Fin
        End If
        objReader = New StreamReader(sPath)

        Label1.Text = "Importation en cours !"
        Label1.Refresh()

        Do
            sLine = objReader.ReadLine()

            If Not sLine Is Nothing Then
                aline = sLine.Split(sSeperator)

                lvi = New ListViewItem(aline)
                Tabl(compt) = lvi.Text

                ListView1.Items.Add(Tabl(compt))

                compt += 1

                ListView1.Refresh()
                ListView1.EnsureVisible(compt - 1)
            End If

        Loop Until sLine Is Nothing
        objReader.Close()

        If ListView1.Items.Count <> 0 Then
            Label1.Text = "Importation finit !"
            Importer.BackColor = Color.Green
            Importer.ForeColor = Color.White
        Else
            Label1.Text = "Erreur fichier txt"
            Label1.Refresh()
        End If
Fin:
    End Function

    ''' <summary>
    ''' Fonction de ping sur l'import du fichier txt
    ''' </summary>
    ''' <returns></returns>
    Function Ftest_ping()
        'Dim a As Integer = ListView1.Items(1).SubItems.Count
        If ListView1.Items(0).SubItems.Count = 2 Then
            ListView1.Clear()

            For i = 0 To compt
                ListView1.Items.Add(Tabl(compt))
            Next

            'Fimporter()
        ElseIf ListView1.Items(0).SubItems.Count = 0 Then
            MsgBox("Importer un fichier txt contenant les adresses IP")
            GoTo Fin
        End If

        Label1.Text = "Test ping en cours !"
        Label1.Refresh()
        ListView1.ForeColor = Color.Black
        For i = 0 To compt - 1
            'array_ip(i) = ListView1.Items(i).Text
            array_ip(i) = Tabl(i)
            Dim ping_result As String
            ping_result = If(My.Computer.Network.Ping(array_ip(i), 4000), "IP TROUVE", "DELAI EXPRIRE")

            ListView1.Items(i).SubItems.Add(ping_result)
            'ListView1.Refresh()

            If ping_result = "IP TROUVE" Then
                ListView1.Items(i).SubItems(0).ForeColor = Color.Green
                'ListView1.Refresh()
            End If
            If ping_result = "DELAI EXPRIRE" Then
                ListView1.Items(i).SubItems(0).ForeColor = Color.Red
                'ListView1.Refresh()
            End If

            ProgressBar1.Value = i * 100 / (compt - 1)
            Label4.Text = ProgressBar1.Value & "%"
            Label4.Refresh()

            ListView1.EnsureVisible(i)
            'ListView1.Refresh()
        Next
        Label1.Text = "Test finit !"
        Test_ping.BackColor = Color.Green
        Test_ping.ForeColor = Color.White
Fin:
    End Function

    ''' <summary>
    ''' Fonction d'export du listview (list du resultat du multiping)
    ''' </summary>
    ''' <returns></returns>
    Function Fexport()
        Label1.Text = "Exportation en cours !"
        Label1.Refresh()

        fichier = "Export_" & Date.Now.Day & "_" & Date.Now.Month & "_" & Date.Now.Year & "_" & Date.Now.Hour & "_" & Date.Now.Minute & ".txt"
        If adresse_fichier = "" Or adresse_fichier = "0" Then
            Fadresse_fichier()
            If adresse_fichier = "0" Then
                GoTo fin
            End If

            rep = MsgBox("Exporter sous le nom de :" & vbCrLf & "'" & fichier & "'" & vbCrLf & vbCrLf & " à cette adresse :" & vbCrLf & adresse_fichier & vbCrLf & vbCrLf & "Valider l'exportation ?", vbYesNo + vbDefaultButton1 + vbQuestion, "Validation")
            If rep = vbNo Then
                GoTo fin
            End If
        End If

        Dim compt1 As Integer = 0
        ProgressBar1.Value = 0
        Using LVStream As New StreamWriter(adresse_fichier & fichier)

            For Each Lvi As ListViewItem In ListView1.Items
                Dim LVRow As String = ""
                For Each LVCell As ListViewItem.ListViewSubItem In Lvi.SubItems
                    LVRow &= LVCell.Text & " - "
                Next
                LVRow = LVRow.Remove(LVRow.Length - 1, 1)
                LVStream.WriteLine(LVRow)

                compt1 += 1
                ProgressBar1.Value = compt1 * 100 / compt
                Label4.Text = ProgressBar1.Value & "%"
                Label4.Refresh()
            Next
        End Using
        Label1.Text = "Exportation faite !"
        Export.BackColor = Color.Green
        Export.ForeColor = Color.White

fin:
    End Function



    ''' <summary>
    ''' Fonction de récupération d'une adresse de dossier windows (C:\Users\...)
    ''' </summary>
    ''' <returns></returns>
    Function Fadresse_fichier()
        Using fBrowser As New FolderBrowserDialog With {
                    .RootFolder = Environment.SpecialFolder.Desktop,
                    .Description = "Sélectionnez un répertoire"
                    }
            fBrowser.ShowDialog()

            If fBrowser.SelectedPath = String.Empty Then
                MsgBox("Pas de sélection")
                adresse_fichier = 0
                Exit Function
            Else
                adresse_fichier = fBrowser.SelectedPath & "\"
            End If
            fBrowser.Dispose()
        End Using

    End Function

    ''' <summary>
    ''' Fonction de récupération du fichier txt
    ''' </summary>
    ''' <returns></returns>
    Function Ffichier()
        OpenFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
        OpenFileDialog1.FilterIndex = 1
        OpenFileDialog1.RestoreDirectory = True
        ' On affiche le formulaire et on teste si l'utilisateur a bien sélectionné un fichier.
        ' L'utilisateur aura donc cliqué sur le bouton OK.
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            ' Récupère le chemin complet du fichier sélectionné par l'utilisateur
            TextBox1.Text = OpenFileDialog1.FileName
            TextBox1.SelectionStart = Len(TextBox1.Text)
            Label1.Text = OpenFileDialog1.SafeFileName
            ' Affiche le nom du fichier (seulement) sélectionné, à l'utilisateur
            'rep = MsgBox("Importer directement ?", vbYesNo + vbDefaultButton1 + vbQuestion, "Validation")
            'If rep = vbNo Then
            '    GoTo fin
            'End If
            'If rep = vbYes Then
            '    Fimporter()
            'End If
            'MsgBox("Vous avez sélectionné le fichier : " & OpenFileDialog1.SafeFileName)

        Else
            ' Si l'utilisateur n'a pas sélectionné de fichier, on lui affiche un avertissement
            MsgBox("Aucun fichier n'a été sélectionné", MsgBoxStyle.Exclamation, "Aucun fichier sélectionné")
        End If

Fin:

        ' Vérifie que le fichier existe
        OpenFileDialog1.CheckFileExists = True
        ' Vérifie que le chemin d'accès du fichier existe
        OpenFileDialog1.CheckPathExists = True



    End Function


    Function File2Compare(File1, File2)
        Const ForReading = 1, ForWriting = 2
        Dim objFSO, objSourceFile, objSourceFile2, vrComprLign
        Dim Titre As String
        Dim vrNumLigne As Integer
        Dim vrLigne, vrLigne2, vrNul, vrLignDif As Object
        Titre = "Fichier à Comparer"
        'File1 = File1.value
        'File2 = File2.value
        Dim bFileNotFound
        bFileNotFound = False
        objFSO = CreateObject("Scripting.FileSystemObject")
        If Not objFSO.FileExists(File1) Then
            MsgBox("Le Fichier N°1 n'existe pas, il faut choisir le Fichier N°1", vbExclamation, Titre)
            bFileNotFound = True
        ElseIf Not objFSO.FileExists(File2) Then
            MsgBox("Le Fichier N°2 n'existe pas, il faut choisir le Fichier N°2", vbExclamation, Titre)
            bFileNotFound = True
        End If
        If Not bFileNotFound Then
            objSourceFile = objFSO.OpenTextFile(File1, ForReading)  'Fichier Original
            objSourceFile2 = objFSO.OpenTextFile(File2, ForReading) 'Fichier Modifié 

            vrNumLigne = 0
            Do Until objSourceFile.AtEndOfStream Or objSourceFile2.AtEndOfStream
                vrNumLigne = vrNumLigne + 1

                vrLigne = objSourceFile.ReadLine
                vrLigne2 = objSourceFile2.ReadLine
                vrComprLign = StrComp(vrLigne, vrLigne2, 1)
                If vrComprLign = 1 Or vrComprLign = -1 Then 'Or vrComprLign = ""
                    vrNul = "Le fichier n'a pas été modifié"
                    vrLignDif = vrLignDif & "ligne n°" & vrNumLigne & " : " & vrLigne & "  ---->  " & vrLigne2 & vbCrLf & vbCrLf
                End If
            Loop
            objSourceFile.Close
            objSourceFile2.Close
            If vrNul = "" Then
                MsgBox("Le fichier n'a pas été modifié ,les deux Fichiers sont identiques !", 64, Titre)
            Else
                ''MsgBox(vrLignDif, 64, Titre)
                'Dim fso, f
                'fso = CreateObject("Scripting.FileSystemObject")
                'f = fso.OpenTextFile("comparaison.txt", ForWriting, True)
                ''f.WriteLine ToString(120, "*")
                'f.writeline("Résultat de la Comparison entre le fichier " & File1 & " et le fichier " & File2 & vbNewLine)
                '' f.WriteLine(String(120, "*"))
                'f.writeline(vrLignDif)
                'txtBody.Text = vrLignDif
                'f.close






                'Création d'un flux d'écriture
                Dim sw As New StreamWriter("C:\Users\Utilisateur\Desktop\CRACK\comparaison.txt")
                'écriture        
                sw.WriteLine("Résultat de la Comparison entre le fichier " & File1 & " et le fichier " & File2 & vbNewLine)
                sw.WriteLine(vrLignDif)
                txtBody.Text = vrLignDif
                sw.Close()
            End If
        End If
    End Function

    Function cmd()
        Dim rx As New Regex("^(?:(?:25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)(?(?=\.?\d)\.)){4}$")

        If Not rx.IsMatch(txt_ping.Text) Then
            MessageBox.Show("L'adresse IP n'est pas au bon format !")
            txt_ping.Clear()
            txt_ping.Focus()
            GoTo fin1
        End If

        '//Setup Shell command for Ping
        '//NOTE: Make to leave a space after PING.exe
        Dim strShellCommand As String
        strShellCommand = "C:\Windows\System32\PING.exe " + txt_ping.Text + " -n 1"

        '//Create Shell Object in to run Scripts
        Dim oSh As Object = CreateObject("WScript.Shell")
        Dim oEx As Object = oSh.Exec(strShellCommand)

        '//Read output buffer
        Dim strBuf As String = oEx.StdOut.ReadAll
        TextBox3.Text = strBuf
fin1:
    End Function

    Function ping_ip(ByVal args As String)

        If args.Length = 0 Then Throw New ArgumentException("Ping needs a host or IP Address.")
        Dim who As String = args
        Dim waiter As AutoResetEvent = New AutoResetEvent(False)
        Dim pingSender As Ping = New Ping()
        AddHandler pingSender.PingCompleted, New PingCompletedEventHandler(AddressOf PingCompletedCallback)
        Dim data As String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        Dim buffer As Byte() = Encoding.ASCII.GetBytes(data)
        Dim timeout As Integer = 4000
        Dim options As PingOptions = New PingOptions(64, True)

        pingSender.SendAsync(who, timeout, buffer, options, waiter)
        'waiter.WaitOne(10000)
        TextBox5.Text = "TTL : " & options.Ttl & vbCrLf & "Ping completed."
        value1 = TextBox5.Text
    End Function

    Function PingCompletedCallback(ByVal sender As Object, ByVal e As PingCompletedEventArgs)
        If e.Cancelled Then
            TextBox5.Text = "Ping canceled." & vbCrLf & CType(e.UserState, AutoResetEvent).[Set]()
        ElseIf e.[Error] IsNot Nothing Then
            TextBox5.Text = "Ping failed:" & vbCrLf & e.[Error].ToString() & vbCrLf & CType(e.UserState, AutoResetEvent).[Set]()
        End If

        Dim reply As PingReply = e.Reply
        Dim value2 As String = DisplayReply(reply)
        TextBox5.Text = value1 & vbCrLf & vbCrLf & value2
        CType(e.UserState, AutoResetEvent).[Set]()
    End Function

    Function DisplayReply(ByVal reply As PingReply)
        Dim b As String
        If reply Is Nothing Then Return reply
        a = reply.Status

        Select Case a
            Case 0
                b = "Ping Good"
            Case 11001
                b = "Buffer Too Small"
            Case 11002
                b = "Net Unreachable"
            Case 11003
                b = "Destination Host Unreachable"
            Case 11004
                b = "Protocol Unreachable"
            Case 11005
                b = "Port Unreachable"
            Case 11006
                b = "No Resources"
            Case 11007
                b = "Bad Option"
            Case 11008
                b = "Hardware Error"
            Case 11009
                b = "Packet Too Big"
            Case 11010
                b = "Request Timed Out"
            Case 11011
                b = "Bad Request"
            Case 11012
                b = "Bad Route"
            Case 11013
                b = "TimeToLive Expired Transit"
            Case 11014
                b = "TimeToLive Expired Reassembly"
            Case 11015
                b = "Parameter Problem"
            Case 11016
                b = "Source Quench"
            Case 11017
                b = "Option Too Big"
            Case 11018
                b = "Bad Destination"
            Case 11032
                b = "Negotiating IPSEC"
            Case 11050
                b = "General Failure"
        End Select

        'TextBox5.Text
        Dim c As String = "ping status : " & a & vbCrLf & vbCrLf & b
        Return c

        If reply.Status = IPStatus.Success Then
            TextBox5.Text = "Adresse Ip: " & reply.Address.ToString() & vbCrLf & "Durée du ping : " & reply.RoundtripTime& & "ms" & vbCrLf & "TTL = " & reply.Options.Ttl & vbCrLf & "Octets = " & reply.Buffer.Length
        End If
    End Function

End Class