﻿Imports System
Imports System.Text
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.ComponentModel
Imports System.Threading

Namespace Examples.System.Net.NetworkInformation.PingTest
    Public Class PingExample
        Public Shared Sub Main(ByVal args As String())
            If args.Length = 0 Then Throw New ArgumentException("Ping needs a host or IP Address.")
            Dim who As String = args(0)
            Dim waiter As AutoResetEvent = New AutoResetEvent(False)
            Dim pingSender As Ping = New Ping()
            AddHandler pingSender.PingCompleted, New PingCompletedEventHandler(AddressOf PingCompletedCallback)
            Dim data As String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            Dim buffer As Byte() = Encoding.ASCII.GetBytes(data)
            Dim timeout As Integer = 12000
            Dim options As PingOptions = New PingOptions(64, True)
            Console.WriteLine("Time to live: {0}", options.Ttl)
            Console.WriteLine("Don't fragment: {0}", options.DontFragment)
            pingSender.SendAsync(who, timeout, buffer, options, waiter)
            waiter.WaitOne()
            Console.WriteLine("Ping example completed.")
        End Sub

        Private Shared Sub PingCompletedCallback(ByVal sender As Object, ByVal e As PingCompletedEventArgs)
            If e.Cancelled Then
                Console.WriteLine("Ping canceled.")
                CType(e.UserState, AutoResetEvent).[Set]()
            End If

            If e.[Error] IsNot Nothing Then
                Console.WriteLine("Ping failed:")
                Console.WriteLine(e.[Error].ToString())
                CType(e.UserState, AutoResetEvent).[Set]()
            End If

            Dim reply As PingReply = e.Reply
            DisplayReply(reply)
            CType(e.UserState, AutoResetEvent).[Set]()
        End Sub

        Public Shared Sub DisplayReply(ByVal reply As PingReply)
            If reply Is Nothing Then Return
            Console.WriteLine("ping status: {0}", reply.Status)

            If reply.Status = IPStatus.Success Then
                Console.WriteLine("Address: {0}", reply.Address.ToString())
                Console.WriteLine("RoundTrip time: {0}", reply.RoundtripTime)
                Console.WriteLine("Time to live: {0}", reply.Options.Ttl)
                Console.WriteLine("Don't fragment: {0}", reply.Options.DontFragment)
                Console.WriteLine("Buffer size: {0}", reply.Buffer.Length)
            End If
        End Sub
    End Class
End Namespace
